import Vue from 'vue'
import App from './App.vue'
import Axios from 'axios'

Vue.config.productionTip = false

Vue.protoype.$auth_http = axios.create({
	baseURL: process.env.AUTH_API_URL,
	timeout: 1000,
});

new Vue({
  render: h => h(App)
}).$mount('#app')
